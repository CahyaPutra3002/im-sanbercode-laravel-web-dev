<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('form.register');
    }

    public function welcome(Request $request)
    {
        $nama = $request['name'];
        $nama2 = $request['name2'];

        return view('form.home' , ['nama' => $nama, 'nama2' => $nama2]);
    }
}
