@extends('layout.master')

:@section('judul')
    BioData Cast 
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-dark btn-sm">Kembali</a>

@endsection