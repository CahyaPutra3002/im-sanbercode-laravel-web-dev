<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label> <br> <br>
        <input type="text" name="name"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text" name="name2"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="staus"> Male <br>
        <input type="radio" name="staus"> Feale <br>
        <input type="radio" name="staus"> Other <br>
        <label>Nationality:</label> <br> <br>
        <select name="Nationality">
            <option value="">Indonesia</option>
            <option value="">Singapur</option>
            <option value="">Malaysia</option>
            <option value="">Saudi Arabia</option>
        </select> <br> <br>
        <label>Languange Spoken:</label> <br> <br>
        <input type="checkbox" name="skill">Bahasa Indonesia <br>
        <input type="checkbox" name="skill">English <br>
        <input type="checkbox" name="skill">Arabic <br>
        <input type="checkbox" name="skill">Japanese <br>
        <input type="checkbox" name="skill">Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea  cols="30" rows="10"></textarea> <br> 

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>