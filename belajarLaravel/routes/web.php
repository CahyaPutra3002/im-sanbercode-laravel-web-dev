<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'bio']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('form.table');
});

Route::get('/data-table', function(){
    return view('form.data');
});


//CRUD cast

//Create
//form untuk menambahkan cast
Route::get('/cast/create', [castController::class, 'create']);
//menyimpan data baru ke table cast
Route::post('/cast', [castController::class, 'store']);


//Read
//menampilkan list data para Cast 
Route::get('/cast', [castController::class, 'index']);
//menampilkan detail data Cast dengan id tertentu
Route::get('/cast/{cast_id}', [castController::class, 'show']);

//Update
//form untuk Meng-update/Edit Cast
Route::get('/cast/{cast_id}/edit', [castController::class, 'edit']);
//menyimpan perubahan data Cast(update) untuk id tertentu
Route::put('/cast/{cast_id}', [castController::class, 'update']);

//Delete
//menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}', [castController::class, 'destroy']);